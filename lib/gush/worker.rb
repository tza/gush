require 'active_job'
require 'redis-mutex'

module Gush
  class Worker < ::ActiveJob::Base
    def perform(workflow_id, job_id, delayed_enqueueing)
      setup_job(workflow_id, job_id, delayed_enqueueing)

      if job.succeeded?
        if delayed_enqueueing
          delayed_enqueue_outgoing_jobs
        else
          # Try to enqueue outgoing jobs again because the last job has redis mutex lock error
          enqueue_outgoing_jobs
        end
        return
      end

      job.payloads = job.include_payload ? incoming_payloads : nil

      error = nil

      if job.running?
        #Avoid having this job removed from the queue
        client.reeschedule_job(workflow_id, job_id, delayed_enqueueing)
      else
        mark_as_started
        begin
          job.perform
        rescue StandardError => error
          mark_as_failed
          raise error
        else
          mark_as_finished
          if delayed_enqueueing
            delayed_enqueue_outgoing_jobs
          else
            enqueue_outgoing_jobs
          end
        end
      end
    end

    private

    attr_reader :client, :workflow_id, :job

    def client
      @client ||= Gush::Client.new(Gush.configuration)
    end

    def setup_job(workflow_id, job_id, delayed_enqueueing)
      @workflow_id = workflow_id
      @job ||= client.find_job(workflow_id, job_id)
      @delayed_enqueueing = delayed_enqueueing
    end

    def incoming_payloads
      jobs = client.find_jobs(workflow_id, job.incoming)
      jobs.map do |job|
        {
          id: job.name,
          class: job.klass.to_s,
          output: job.output_payload
        }
      end
    end

    def mark_as_finished
      job.finish!
      client.persist_job(workflow_id, job)
    end

    def mark_as_failed
      job.fail!
      client.persist_job(workflow_id, job)
    end

    def mark_as_started
      job.start!
      client.persist_job(workflow_id, job)
    end

    def elapsed(start)
      (Time.now - start).to_f.round(3)
    end

    def delayed_enqueue_outgoing_jobs
      client.add_to_delayed_enqueue(job.workflow_id, job.outgoing) if job.outgoing.any?
    end

    def enqueue_outgoing_jobs
      jobs = client.find_jobs(job.workflow_id, job.outgoing)
      jobs.each do |job|
        if job.ready_to_start?
          RedisMutex.with_lock("gush_enqueue_outgoing_jobs_#{job.workflow_id}-#{job.name}", sleep: 0.2, block: 2) do
            client.enqueue_job(job.workflow_id, job, false)
          end
        end
      end
    rescue RedisMutex::LockError
      Worker.set(wait: 2.seconds).perform_later(workflow_id, job.name, false)
    end

  end
end
