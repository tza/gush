require 'redis'
require 'concurrent-ruby'

module Gush
  class Client
    attr_reader :configuration

    @@redis_connection = Concurrent::ThreadLocalVar.new(nil)

    def self.redis_connection(config)
      cached = (@@redis_connection.value ||= config.redis_opts.merge({ connection: nil }))
      return cached[:connection] if !cached[:connection].nil? && config.redis_opts[:url] == cached[:url]

      # Support 4.2 and 4.1 https://github.com/redis/redis-rb/blame/v4.2.1/CHANGELOG.md
      # These versions support ruby > 2.2
      #if Gem::Version.new(RUBY_VERSION) >= Gem::Version.new('2.3.0')
        #Redis.exists_returns_integer = true
      #end

      Redis.new(config.redis_opts).tap do |instance|
        RedisClassy.redis = instance
        @@redis_connection.value = config.redis_opts.merge({ connection: instance })
      end
    end

    def initialize(config = Gush.configuration)
      @configuration = config
    end

    def configure
      yield configuration
    end

    def create_workflow(name)
      begin
        name.constantize.create
      rescue NameError
        raise WorkflowNotFound.new("Workflow with given name doesn't exist")
      end
      flow
    end

    def start_workflow(workflow, job_names = [])
      workflow.mark_as_started
      persist_workflow(workflow)
      set_workflow_ttl(workflow) if default_ttl.positive?
      if job_names.empty?
        jobs = workflow.initial_jobs
        if workflow.delayed_enqueueing
          job_names = jobs.map(&:name)
          add_to_delayed_enqueue(workflow.id, job_names)
          enqueue_enqueuer_worker(workflow.id, workflow.enqueuer_frequency)
        else
          enqueue_jobs(workflow.id, jobs, workflow.delayed_enqueueing)
        end
      else
        job_names.map {|name| workflow.find_job(name) }
        enqueue_jobs(workflow.id, jobs, workflow.delayed_enqueueing)
      end
    end

    def stop_workflow(id)
      workflow = find_workflow(id, skip_jobs: true)
      workflow.mark_as_stopped
      persist_workflow(workflow)
    end

    def next_free_job_id(workflow_id, job_klass)
      @job_ids ||= Set.new
      job_id = nil

      loop do
        job_id = SecureRandom.uuid
        available = !@job_ids.include?(job_id)

        break if available
      end

      job_id
    end

    def next_free_workflow_id
      id = nil
      loop do
        id = SecureRandom.uuid
        available = !redis.exists("gush.workflow.#{id}")
        break if available
      end

      id
    end

    def all_workflows
      redis.scan_each(match: "gush.workflows.*").map do |key|
        id = key.sub("gush.workflows.", "")
        find_workflow(id, skip_jobs: true)
      end
    end

    def find_workflow(id, skip_jobs: false)
      data = redis.get("gush.workflows.#{id}")

      unless data.nil?
        hash = Gush::JSON.decode(data, symbolize_keys: true)

        if skip_jobs
          nodes = []
        else
          keys = redis.scan_each(match: "gush.jobs.#{id}.*")
          nodes = keys.each_with_object([]) do |key, array|
            array.concat redis.hvals(key).map { |json| Gush::JSON.decode(json, symbolize_keys: true) }
          end
        end

        workflow_from_hash(hash, nodes)
      else
        raise WorkflowNotFound.new("Workflow with given id doesn't exist")
      end
    end

    def persist_workflow(workflow)
      redis.set("gush.workflows.#{workflow.id}", workflow.to_json)
      persist_jobs(workflow.id, workflow.jobs)
      workflow.mark_as_persisted
      true
    end

    def persist_job(workflow_id, job)
      redis.hset("gush.jobs.#{workflow_id}.#{job.klass}", job.id, job.to_json)
    end

    def persist_jobs(workflow_id, jobs)
      jobs_by_klass = jobs.group_by { |job| job.klass }
      jobs_by_klass.each do |job_klass, jobs|
        jobs.each_slice(write_batch_size) do |jobs_batch|
          jobs_h = {}
          jobs_batch.each do |job|
            jobs_h[job.id] = job.to_json
          end
          redis.hset("gush.jobs.#{workflow_id}.#{job_klass}", jobs_h)
        end
      end
    end

    def find_job(workflow_id, job_name)
      job_name_match = /(?<klass>\w*[^-])-(?<identifier>.*)/.match(job_name)

      data = if job_name_match
               find_job_by_klass_and_id(workflow_id, job_name)
             else
               find_job_by_klass(workflow_id, job_name)
             end

      return nil if data.nil?

      data = Gush::JSON.decode(data, symbolize_keys: true)
      Gush::Job.from_hash(data)
    end

    def find_jobs(workflow_id, job_names)
      partitions = job_names.partition do |job_name|
        /(?<klass>\w*[^-])-(?<identifier>.*)/.match(job_name)
      end

      jobs_to_be_retrieved_by_klass_and_id = partitions.first
      jobs_to_be_retrieved_by_klass_only = partitions.last

      jobs = []

      jobs = find_jobs_by_klass_and_id(workflow_id, jobs_to_be_retrieved_by_klass_and_id)
      jobs_to_be_retrieved_by_klass_only.each do |job_name|
        job = find_job_by_klass(workflow_id, job_name)
        jobs << job if job
      end

      jobs.map do |job_data|
        data = Gush::JSON.decode(job_data, symbolize_keys: true)
        Gush::Job.from_hash(data)
      end
    end

    def destroy_workflow(workflow)
      redis.del("gush.workflows.#{workflow.id}")
      klasses = workflow_job_klasses(workflow).each { || }
      klasses.each { |klass| destroy_jobs_klass(workflow.id, klass) }
    end

    def expire_workflow(workflow, ttl=nil)
      ttl = ttl || default_ttl
      redis.expire("gush.workflows.#{workflow.id}", ttl)
      workflow_job_klasses(workflow).each { |klass| set_job_klass_ttl(workflow.id, klass, ttl) }
    end
    alias_method :set_workflow_ttl, :expire_workflow

    def expire_job(workflow_id, job, ttl=nil)
      ttl = ttl || default_ttl
      redis.expire("gush.jobs.#{workflow_id}.#{job.klass}", ttl)
    end

    def add_to_delayed_enqueue(workflow_id, job_names)
      ttl = default_ttl
      redis.sadd("gush.enqueue.#{workflow_id}", job_names)
      redis.expire("gush.enqueue.#{workflow_id}", ttl)
    end

    def jobs_to_be_enqueued(workflow_id)
      job_names = redis.smembers("gush.enqueue.#{workflow_id}")
      find_jobs(workflow_id, job_names)
    end

    def remove_from_delayed_enqueue_queue(workflow_id, job_names)
      redis.srem("gush.enqueue.#{workflow_id}", job_names)
    end

    def enqueue_job(workflow_id, job, delayed_enqueueing)
      job.enqueue!
      persist_job(workflow_id, job)
      queue = job.queue || configuration.namespace

      Gush::Worker.set(queue: queue).perform_later(workflow_id, job.name, delayed_enqueueing)
    end

    def enqueue_jobs(workflow_id, jobs, delayed_enqueueing)
      jobs.each(&:enqueue!)
      persist_jobs(workflow_id, jobs)
      jobs.each do |job|
        queue = job.queue || configuration.namespace
        Gush::Worker.set(queue: queue).perform_later(workflow_id, job.name, delayed_enqueueing)
      end
    end

    def reeschedule_job(workflow_id, job_id, delayed_enqueueing)
      Worker.set(queue: configuration.namespace, wait: configuration.enqueuer_frequency).perform_later(workflow_id, job_id, delayed_enqueueing)
    end

    def enqueue_enqueuer_worker(workflow_id, custom_frequency=nil)
      EnqueuerWorker.set(queue: configuration.namespace, wait: custom_frequency || configuration.enqueuer_frequency).perform_later(workflow_id)
    end

    private

    def set_job_klass_ttl(workflow_id, job_klass, ttl)
      redis.expire("gush.jobs.#{workflow_id}.#{job_klass}", ttl)
    end

    def destroy_jobs_klass(workflow_id, job_klass)
      redis.del("gush.jobs.#{workflow_id}.#{job_klass}")
    end

    def workflow_job_klasses(workflow)
      workflow.jobs.map { |job| job.klass }.uniq
    end

    def default_ttl
      configuration.ttl
    end

    def write_batch_size
      configuration.write_batch_size
    end

    def read_batch_size
      configuration.read_batch_size
    end

    def find_job_by_klass_and_id(workflow_id, job_name)
      job_klass, job_id = job_name.split('|')
      redis.hget("gush.jobs.#{workflow_id}.#{job_klass}", job_id)
    end

    def find_jobs_by_klass_and_id(workflow_id, job_names)
      job_ids_by_klass = job_names.each_with_object(Hash.new{ |h,k| h[k] = [] }) do |job_name, acc|
        job_klass, job_id = job_name.split('|')
        acc[job_klass] << job_id
      end

      jobs = []
      job_ids_by_klass.each do |job_klass, job_ids|
        job_ids.each_slice(read_batch_size) do |job_ids_batch|
          jobs.push(*redis.hmget("gush.jobs.#{workflow_id}.#{job_klass}", *job_ids_batch))
        end
      end
      jobs
    end

    def find_job_by_klass(workflow_id, job_name)
      new_cursor, result = redis.hscan("gush.jobs.#{workflow_id}.#{job_name}", 0, count: 1)
      return nil if result.empty?

      job_id, job = *result[0]

      job
    end

    def workflow_from_hash(hash, nodes = [])
      delayed_enqueueing = hash.fetch(:delayed_enqueueing, false)
      flow = hash[:klass].constantize.new(*hash[:arguments], skip_job_generation: true, delayed_enqueueing: delayed_enqueueing)
      flow.stopped = hash.fetch(:stopped, false)
      flow.id = hash[:id]

      nodes.map do |node|
        flow.load_job(Gush::Job.from_hash(node))
      end

      flow
    end

    def redis
      self.class.redis_connection(configuration)
    end
  end
end
