module Gush
  class Configuration
    attr_accessor :concurrency, :namespace, :redis_opts, :ttl, :enqueuer_frequency, :read_batch_size, :write_batch_size

    def self.from_json(json)
      new(Gush::JSON.decode(json, symbolize_keys: true))
    end

    def initialize(hash = {})
      self.concurrency = hash.fetch(:concurrency, 5)
      self.namespace = hash.fetch(:namespace, 'gush')
      self.redis_opts = hash.fetch(:redis_opts, { url: 'redis://localhost:6379', :connect_timeout => 0.5, :read_timeout => 1, :write_timeout => 2 })
      self.gushfile = hash.fetch(:gushfile, 'Gushfile')
      self.ttl = hash.fetch(:ttl, -1)
      self.write_batch_size = hash.fetch(:write_batch_size, 10000)
      self.read_batch_size = hash.fetch(:read_batch_size, 10000)
      self.enqueuer_frequency = hash.fetch(:enqueuer_frequency, 10.seconds)
    end

    def gushfile=(path)
      @gushfile = Pathname(path)
    end

    def gushfile
      @gushfile.realpath if @gushfile.exist?
    end

    def to_hash
      {
        concurrency:          concurrency,
        namespace:            namespace,
        redis_opts:           redis_opts,
        ttl:                  ttl,
        enqueuer_frequency: enqueuer_frequency,
        write_batch_size: write_batch_size,
        read_batch_size:  read_batch_size
      }
    end

    def to_json
      Gush::JSON.encode(to_hash)
    end
  end
end
